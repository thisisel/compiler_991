## Compiler Project

```
.
├── dfa
│   ├── first_lang.py  # even a and even b
│   ├── __init__.py
│   └── second_lang.py # only b{2} and a+
├── pyparser
│   ├── __init__.py
│   ├── py_lex.py
│   ├── py_parse.py
│  
├── README.md
├── requirements.txt
```
