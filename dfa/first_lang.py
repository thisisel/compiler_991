class EvenAutomata:
    """
    This class implements instances
    of a 2 state DFA, each instance
    concerned about a single Alphabet .

    ACCEPTED STATE :
    ----Even number of Alphabets

    By adhering to the closure properties
    of regular langages agains union,
    this class can be expanded to cover
    languages such as
    ----L1 UNION L2 = L3

    in which Li is
    ----L1: Even number of Alphabet = a
    ----L2: Even number of Alphabet = b

    """

    auto_states = [("even", True), ("odd", False)]
    init_state = auto_states[0]

    def __init__(self, alphabet):
        self.alphabet = alphabet
        self.current_state = self.init_state

    def transit(self, in_char) -> tuple:
        if self.current_state[1] and in_char == self.alphabet:
            self.current_state = self.auto_states[1]
        elif self.current_state[1] is False and in_char == self.alphabet:
            self.current_state = self.auto_states[0]
        return self.current_state


def start_scan(input_str, a_auto, b_auto):
    str_state = True
    for alphabet in input_str:
        a_state = a_auto.transit(alphabet)
        b_state = b_auto.transit(alphabet)
        str_state = a_state[1] and b_state[1]
    return str_state


def main():
    while True:
        a_auto = EvenAutomata("a")
        b_auto = EvenAutomata("b")

        input_str = input(
            "****\nPlease enter your input string: \n"
        )

        result = start_scan(input_str, a_auto, b_auto)
        print(f' "{input_str}" is {result} to language')


if __name__ == "__main__":
    main()
