class SecondLanguage:
    """
        This class implements a 7 state DFA 
        for language L such that 
            every string that has
        ------at least 1 'a' (a+)
        ------and
        ------only 2 'b' (b{2})
        --->is Accepted
    """
    states = ["a0_b0", "a1_b0", "a0_b1", "a1_b1", "a0_b2", "a1_b2", "trap"]
    init_state = states[0]
    accept_state = states[5]
    
    # map input __  key : current_state __ value: next_state
    a_current_next = {"a0_b0": "a1_b0",
                      "a1_b0": "a1_b0",
                      "a0_b1": "a1_b1",
                      "a1_b1": "a1_b1",
                      "a0_b2": "a1_b2",
                      "a1_b2": "a1_b2",
                      "trap": "trap"
                      }
    b_current_next = {"a0_b0": "a1_b1",
                      "a1_b0": "a1_b1",
                      "a0_b1": "a1_b2",
                      "a1_b1": "a1_b2",
                      "a0_b2": "trap",
                      "a1_b2": "trap",
                      "trap": "trap"
                      }

    def __init__(self):
        self.current_state = self.init_state

    def a_transit(self, in_char):
        """
            returns next state for input a
            by using current state as key 
            in mapping to next state as value
        """
        return self.a_current_next.get(self.current_state)

    def b_transit(self, in_char):
        """
            returns next state for input b
            by using current state as key 
            in mapping to next state as value
        """
        return self.b_current_next.get(self.current_state)

    def master_transit(self, in_char):
        """
            substitutes current state with next state 
            based on input char
        """
        if in_char == "a":
            self.current_state = self.a_transit(in_char)
        elif in_char == "b":
            self.current_state = self.b_transit(in_char)
        else:
            raise Exception("Input character not defined in alphabet")

    def evaluate_input(self, input_str):
        for char in input_str:
            self.master_transit(char)

        return True if self.current_state == self.accept_state else False   


def main():
    second_lang = SecondLanguage()
    input_str = input('Enter your string: \n')
    result = second_lang.evaluate_input(input_str)
    print(f'{input_str} is {result} to the second language')


if __name__ == "__main__":
    main()
