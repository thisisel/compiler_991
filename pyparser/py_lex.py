import ply.lex as lex
class PyLex:
    """
    This class implements a simple lexical 
    for Python analyzer by importing ply.lex

    There are essentially 4 token classes recognized, 
    each having distinct subtypes abiding by Regex rules.
    Major Token Classes:
        - id : name of classes, variables, functions ...
        - keyword : reserved words
        - literal : value given to variables
        - operator
        - delimeters
"""

# list of all the tokens regardless of their class
    tokens = [
        'NUMBER',
        'PLUS',
        'MINUS',
        'STARSTAR',
        'STAR',
        'DIVIDE',
        'LPRN',
        'RPRN',
        'ID',
        'LBRACE',
        'RBRACE',
        'LBRAK',
        'RBRAK',
        'DOT',
        'EQUAL',
        'EQEQUAL',
        'COMMA',
        'COLON',
        'INDENT',
        'DOUBLESTAR',
        'LSQB',
        'RSQB',
        'LPAR',
        'RPAR',
        'LESS',
        'GREATER',
        'AMPER',
        'PERCENT',
        'SEMI',
        'AT',
        'KEYWORD',
        'LITERAL',
        'NEWLINE',
        'LSHIFT',
        'RSHIFT',
        'DDIVIDE'
    ]

    """
        handling keywords
        by mapping lexemes with tokens
    """
    reserved = {'False': 'FALSE',
                'else': 'ELSE',
                'import': 'IMPORT',
                'pass': 'PASS',
                'None': 'NONE',
                'break': 'BREAK',
                'in': 'IN',
                'True': 'TRUE',
                'class': 'CLASS',
                'is': 'IS',
                'return': 'RETURN',
                'yield':'YIELD',
                'raise' : 'RAISE',
                'and': 'AND',
                'continue': 'CONTINUE',
                'for': 'FOR',
                'lambda': 'LAMBDA',
                'as': 'AS',
                'def': 'DEF',
                'from': 'FROM',
                'while': 'WHILE',
                'del': 'DEL',
                'not': 'NOT',
                'with': 'WITH',
                'elif': 'ELIF',
                'if': 'IF',
                'or': 'OR',
                }

    tokens.extend(reserved.values())

    """
        Operators
    """
    t_PLUS = r'\+'
    t_MINUS = r'-'
    t_DOUBLESTAR = r'\*\*'
    t_STAR = r'\*'
    t_DIVIDE = r'/'
    t_DDIVIDE = r'//'
    t_EQEQUAL = r'=='
    t_LESS = r'<'
    t_GREATER = r'>'
    t_AMPER = r'&'
    t_PERCENT = r'%'
    r_NOTEQUAL = r'!='
    r_LSHIFT = r'<<'
    r_RSHIFT = r'>>'

    """
        Delimiters
    """
    t_EQUAL = r'='
    t_LBRACE = r'\{'
    t_RBRACE = r'\}'
    t_LSQB = r'\['
    t_RSQB = r'\]'
    t_DOT = r'\.'
    t_LPAR = r'\('
    t_RPAR = r'\)'
    t_COMMA = r','
    t_COLON = r':'
    t_SEMI = r';'
    t_AT = r'@'

    t_ignore = ' \t'

    """
        Literals:
            - NUMBER (INTEGER/FLOAT/IMAG)
            - STR
            - BOOL
    """


    def t_NUMBER(self, t):
        r'\d+'
        t.value = int(t.value)
        return t

    def t_newline(self, t):
        r'\n+'
        t.lexer.lineno += len(t.value)

    def t_ID(self, t):
        r'[a-zA-Z_][a-zA-Z_0-9]*'
        t.type = self.reserved.get(t.value, 'ID')    # Check for reserved words
        return t


    def t_error(self, t):
        """
        error handling
        """
        print("Illegal character '%s'" % t.value[0])
        t.lexer.skip(1)

    def get_tokens(self):
        return self.tokens

    def build(self,**kwargs):
        self.lexer = lex.lex(module=self, **kwargs)

    def start_scan(self,data):
        self.lexer.input(data)

        for tok in self.lexer:
            print(tok)
    

def main():
    source = input('please enter your source: \n')

    lex_instance = PyLex()
    lex_instance.build()
    lex_instance.start_scan(source)


if __name__ == '__main__':
    main()

else:
      import_instance = PyLex() 
      import_instance.build()
