import ply.yacc as yacc
import py_lex

tokens = py_lex.import_instance.get_tokens()


def p_shift_expr(p):
    """
    shift_expr : shift_expr LSHIFT sum
                | shift_expr RSHIFT sum
                | sum
    """
    if p[2] == "<<":
        p[0] = p[1] << p[3]
    elif p[2] == ">>":
        p[0] == p[1] >> p[3]
    else:
        p[0] = p[1]


def p_sum(p):
    """
    sum : sum PLUS term
        | sum MINUS term
        | term
    """
    if p[2] == "+":
        p[0] = p[1] + p[3]
    elif p[2] == "-":
        p[0] = p[1] - p[3]
    else:
        p[0] = p[1]


def p_term(p):
    """
    term :
         | term STAR factor
         | term DIVIDE factor
         | term DDIVIDE factor
         | term PERCENT factor
         | term AT factor
         | factor
    """
    if p[2] == "*":
        p[0] = p[1] * p[3]
    elif p[2] == "/":
        p[0] = p[1] / p[3]
    elif p[2] == "//":
        p[0] = p[1] // p[3]
    elif p[2] == "%":
        p[0] = p[1] % p[3]
    elif p[2] == "@":
        p[0] = p[1] @ p[3]
    else:
        p[0] = p[1]


def p_factor(p):
    "factor : NUMBER"
    p[0] = p[1]


def p_pass_stmt(p):
    "pass_stmt : PASS"
    p[0] = p[1]


def p_break_stmt(p):
    "break_stmt : BREAK"
    p[0] = p[1]


def p_continue_stmt(p):
    "continue_stmt : CONTINUE"
    p[0] = p[1]


def p_error(p):
    print("Syntax error in input!")


def main():
    parser = yacc.yacc()
    source = input("enter your source\n")

    while True:
        try:
            s = source
        except EOFError:
            break
        if not s:
            continue
        result = parser.parse(s)
        print(result)


if __name__ == "__main__":
    main()
